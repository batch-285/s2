package com.zuitt.WD043_S2_A2;

import java.util.ArrayList;
import java.util.HashMap;

public class PrimeNumbers {
    public static void main(String[] args) {
        int[] primeNumbers = {2, 3, 5, 7, 11};

        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("Itsy");
        stringList.add("Bitsy");
        stringList.add("Tinny");
        stringList.add("Winnie");

        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Yellow", 100);
        hashMap.put("Polka", 120);
        hashMap.put("Dots", 130);

        System.out.println("My friends are: " + stringList);
        System.out.println("Our current inventory consist of: " + hashMap);
    }
}
