package com.zuitt.WDC043_S2_A1;

import java.util.Scanner;

public class A1Solution {
    public static void main(String[] args) {
        Scanner leapYear = new Scanner(System.in);

        System.out.println("Enter a year:");
        int year = leapYear.nextInt();

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.println("The year " + year + " is a leap year.");
                } else {
                    System.out.println("The year " + year + " is NOT a leap year.");
                }
            } else {
                System.out.println("The year " + year + " is a leap year.");
            }
        } else {
            System.out.println("The year " + year + " is NOT a leap year.");
        }
    }
}
