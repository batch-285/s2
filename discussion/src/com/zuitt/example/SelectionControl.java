package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        int num1 = 36;
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5.");
        } else {
            System.out.println(num1 + " is not divisible by 5.");
        }

//        int x = 15;
//        int y = 0;
//
//        if (y != && x/y == 0) {
//            System.out.println("Result is: " + x/y);
//        } else {
//            System.out.println("This will only run because of short circuiting");
//        }

        int num2 = 24;
        Boolean result = (num2>0)?true:false;
        System.out.println(result);

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number:");
        int directionValue = numberScanner.nextInt();

        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
