package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        System.out.println(Arrays.toString(intArray));

        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

        Arrays.sort(intArray);
        System.out.println("Sorted Array: " + Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        classroom[0][0] = "Naruto";
        classroom[0][1] = "Sasuke";
        classroom[0][2] = "Sakura";

        classroom[1][0] = "linny";
        classroom[1][1] = "Tuck";
        classroom[1][2] = "Ming-ming";

        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermoine";
        System.out.println(Arrays.deepToString(classroom));

        ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane", "Mike"));
        students.add("Cardo");
        students.add("Luffy");
        System.out.println(students);

        System.out.println(students.get(3));

        students.add(0, "Cardi");
        System.out.println(students.get(0));
        System.out.println(students);

        students.set(1, "Tom");
        System.out.println(students);

        students.remove(1);
        System.out.println(students);

        students.clear();
        System.out.println(students);

        System.out.println(students.size());

        HashMap<String, String> jobPosition = new HashMap<>(){
            {
                put("Teacher", "Cee");
                put("Web Developer", "Peter Parker");
            }
        };
        jobPosition.put("Dreamer", "Morpheus");
        jobPosition.put("Police", "Cardo");
        System.out.println(jobPosition);
        System.out.println(jobPosition.get("Police"));
        System.out.println(jobPosition.replace("Dreamer", "Persephone"));
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);
        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());

    }
}
